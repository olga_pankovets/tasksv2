var gulp = require('gulp'),
concatCSS = require('gulp-concat-css'),
cleanCSS = require('gulp-clean-css'),
rename = require('gulp-rename'),
concatScript = require('gulp-concat'),
jsmin = require('gulp-jsmin');

gulp.task('default', function () {
  return gulp.src('Content/*.css')
    .pipe(concatCSS('allstyles.css'))
    .pipe(gulp.dest('Content/compact'))
   	.pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename('allstyles.min.css'))
    .pipe(gulp.dest('Content/compact'));
});

gulp.task("scripts", function() {
	gulp.src("Scripts/*.js")
		.pipe(concatScript('allscripts.js'))
		.pipe(gulp.dest("Scripts/compact/"))
		.pipe(jsmin())
		.pipe(rename('allscripts.min.js'))
		.pipe(gulp.dest("Scripts/compact/"))
}); 

gulp.task('watch', function () {
  gulp.watch('Content/*.css', ['default']);
  gulp.watch('Scripts/*.js', ['scripts']); 
});




