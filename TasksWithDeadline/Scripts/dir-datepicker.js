﻿angular.module('toDoApp').directive("datepicker", function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            $(function () {
                element.datepicker({
                    dateFormat: 'mm/dd/yy',
                    onSelect: function (date) {
                        scope.task.Deadline = date;
                        scope.task.IsOutdated = new Date(date) < new Date();
                        console.log(scope.task.IsOutdated);
                        console.log(typeof (date));
                        scope.changeTask(scope.task);
                    }
                });
            });
        }
    }
});
