﻿var app = angular.module('toDoApp', []);

app.controller('taskCtrl', function ($scope, $http) {
    $scope.tasks = [];

    //get all records from database when first loading the page
    $http.get("/tasks/method-get").then(function mySucces(response) {
        $scope.tasks = response.data.map(function (task) {
            task.IsOutdated = (new Date(task.Deadline) < new Date());
            return task;
        });
    });

    $scope.newTaskName = "";
    $scope.tasksFilter = 'none';

    //add new task to database and receive it's id in the database in return
    $scope.addTask = function ($event) {
        if ($scope.newTaskName && ($event.type == 'click' || ($event.type == 'keyup' && $event.keyCode == 13))) {
            var newTask = { TaskId: 0, Name: $scope.newTaskName, IsCompleted: false, Deadline: new Date() };
            var addData = angular.toJson(newTask);
            $http.post("/api/Values", addData).then(function mySucces(response) {
                newTask.TaskId = parseInt(response.data);
                $scope.tasks.push(newTask);
                $scope.newTaskName = "";
            });
        }
    }

    //delete task by it's id
    $scope.deleteTask = function (task) {
        var index = $scope.tasks.indexOf(task);
        $scope.tasks.splice(index, 1);
        $http.delete('/api/Values/' + task.TaskId).then(function mySucces(response) {});
    }

    //on changing any property we update the corresponding record in the database
    $scope.changeTask = function (task) {
        var changeData = angular.toJson(task);
        $http.put('/api/Values/' + task.TaskId, changeData).then(function mySucces(response) { });
    }

    $scope.clearCompleted = function () {
        for (var i = $scope.tasks.length - 1; i >= 0; i--) {
            var task = $scope.tasks[i];
            if (task.IsCompleted == true) {
                $scope.tasks.splice(i, 1);
                $http.delete('/api/Values/' + task.TaskId).then(function mySucces(response) { });
            }
        };
        return;
    }
});


