﻿angular.module('toDoApp').filter('completedFilter', function () {
    return function (input, filter) {
        if (filter == 'none') {
            return input;
        }
        else {
            var result = [];
            angular.forEach(input, function (task) {
                if (task.IsCompleted == filter) {
                    result.push(task);
                }
            });
            return result;
        }
    };
});