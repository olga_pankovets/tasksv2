﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using TasksWithDeadline.Models;


namespace TasksWithDeadline.Controllers
{

    [RoutePrefix("tasks")]
    public class ValuesController : ApiController
    {
        private TaskContext db = new TaskContext();
        [HttpGet]
        [Route("method-get")]
        [EnableQuery]
        public IQueryable<Task> Get()
        {
            return db.Tasks;
        }


        public string Post([FromBody]Task value)
        {
            if (value != null)
            {
                db.Tasks.Add(value);
                db.SaveChanges();
                return JsonConvert.SerializeObject(value.TaskId);
            }
            return JsonConvert.SerializeObject(-1);
        }

        public void Put(int id, [FromBody]Task value)
        {
            if (value != null)
            {
                var record = db.Tasks.SingleOrDefault(rec => rec.TaskId == id);
                if (record != null)
                {
                    record.Name = value.Name;
                    record.IsCompleted = value.IsCompleted;
                    record.Deadline = value.Deadline;
                    db.SaveChanges();
                }
                return;
            }
        }

        public void Delete(int id)
        {
            Task toDelete = db.Tasks.SingleOrDefault(rec => rec.TaskId == id);
            if (toDelete != null)
            {
                db.Tasks.Remove(toDelete);
            }
            db.SaveChanges();
        }

    }
}
