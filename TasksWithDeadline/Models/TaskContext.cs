﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TasksWithDeadline.Models
{
    public class TaskContext : DbContext
    {
        public TaskContext()
            : base("DBConnection")
        { }
          
        public DbSet<Task> Tasks { get; set; }
    }
}