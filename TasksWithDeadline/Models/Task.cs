﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TasksWithDeadline.Models
{
    public class Task
    {
        public int TaskId { get; set; }
        public string Name { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime Deadline { get; set; }
    }
}